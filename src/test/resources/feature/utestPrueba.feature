#Autor: Miguel Alvarez
  @stories
  Feature: Registro de un usuario en la pagina de utest
    @Scenario
    Scenario: Miguel se quiere registrar en la pagina de utest
      Given El usuario desea registrarse en utest
      Then El usuario diligencia todos los datos para el registro
      |strNombre|strApellido|strEmail            |strMes|strDia|strAno|strCiudad|strZip|strPais |strComputador|strVersion|strLenguaje|strMovil|strModelo|strOs   |strContrasena|strContrasenaConfirmar|
      |Miguel   |Alvarez    |Linxus3220@gmail.com|May   |22    |1990  |Sincelejo|700001|Colombia|Windows      |10        |Spanish    |Apple   |iPhone X |iOS 14.4|123456789/*-+|123456789/*-+         |
      When El registro se completa cuando el usuario ve el boton de Complete Setup
      |strBotonFinal |
      |Complete Setup|