package co.com.utestPrueba.Stepdefinitions;

import co.com.utestPrueba.model.UtestModelo;
import co.com.utestPrueba.questions.Respuesta;
import co.com.utestPrueba.tasks.*;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.*;

public class Stepdefinitios {

    @Before
    public void setOnStage(){
        setTheStage(new OnlineCast());
    }

    @Given("^El usuario desea registrarse en utest$")
    public void elUsuarioDeseaRegistrarseEnUtest() {
        theActorCalled("Miguel").attemptsTo(AbrirPagina.enLaPagina());
    }

    @Then("^El usuario diligencia todos los datos para el registro$")
    public void elUsuarioDiligenciaTodosLosDatosParaElRegistro(List<UtestModelo> datos){
        theActorInTheSpotlight().attemptsTo(
                LlenarInformacionPersonal.enLaPagina(datos),
                LlenarDireccion.enLaPagina(datos),
                LlenarDispositivos.enLaPagina(datos),
                LlenarContrasena.enLaPgina(datos)
                );
    }

    @When("^El registro se completa cuando el usuario ve el boton de Complete Setup$")
    public void elRegistroSeCompletaCuandoElUsuarioVeElBotonDeCompleteSetup(List<UtestModelo> datos) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(Respuesta.es(datos)));

    }
}
