package co.com.utestPrueba.tasks;


import co.com.utestPrueba.model.UtestModelo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;


import java.util.List;

import static co.com.utestPrueba.userinterfaces.PaginaIinformacionPersonal.*;

public class LlenarInformacionPersonal implements Task {
    private List<UtestModelo> datos;

    public LlenarInformacionPersonal(List<UtestModelo> datos) {
        this.datos = datos;
    }

    public static LlenarInformacionPersonal enLaPagina(List<UtestModelo> datos) {
        return Tasks.instrumented(LlenarInformacionPersonal.class, datos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BOTON_JOIN),
                Enter.theValue(datos.get(0).getStrNombre()).into(CAMPO_NOMBRE),
                Enter.theValue(datos.get(0).getStrApellido()).into(CAMPO_APELLIDO),
                Enter.theValue(datos.get(0).getStrEmail()).into(CAMPO_EMAIL),
                SelectFromOptions.byVisibleText(datos.get(0).getStrMes()).from(CAMPO_MES),
                SelectFromOptions.byVisibleText(datos.get(0).getStrDia()).from(CAMPO_DIA),
                SelectFromOptions.byVisibleText(datos.get(0).getStrAno()).from(CAMPO_ANO),
                Click.on(BOTON_NEXT)

        );

    }
}
