package co.com.utestPrueba.tasks;

import co.com.utestPrueba.model.UtestModelo;
import static co.com.utestPrueba.userinterfaces.PaginaDireccion.*;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.openqa.selenium.Keys;

import java.util.List;

public class LlenarDireccion implements Task {
    private List<UtestModelo> datos;

    public LlenarDireccion(List<UtestModelo> datos) {
        this.datos = datos;
    }

    public static LlenarDireccion enLaPagina(List<UtestModelo> datos) {
        return Tasks.instrumented(LlenarDireccion.class,datos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(datos.get(0).getStrCiudad()).into(CAMPO_CIUDAD),
                Enter.theValue(datos.get(0).getStrZip()).into(CAMPO_ZIP),
                Click.on(DIV_PAIS),
                Enter.theValue(datos.get(0).getStrPais()).into(CAMPO_PAIS).thenHit(Keys.ARROW_DOWN,Keys.ENTER),
                Click.on(BOTON_NEXT)
        );

    }
}
