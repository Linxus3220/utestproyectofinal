package co.com.utestPrueba.questions;

import co.com.utestPrueba.model.UtestModelo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static co.com.utestPrueba.userinterfaces.PaginaContrasena.BOTON_COMPLETE_SETUP;

public class Respuesta implements Question<Boolean> {
    private List<UtestModelo> datos;

    public Respuesta(List<UtestModelo> datos) {
        this.datos = datos;
    }

    public static Respuesta es(List<UtestModelo> datos) {
        return new Respuesta(datos);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        String texto_boton = Text.of(BOTON_COMPLETE_SETUP).viewedBy(actor).asString();
        return datos.get(0).getStrBotonFinal().equals(texto_boton);
    }
}
